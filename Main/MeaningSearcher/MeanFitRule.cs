﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mean
{
    public class MeanFitRule
    {
        public MeanFitRule(Meaning mean, double lowTrustProximity = 0.6, int priority = 0, params string[] stopMeanings)
        {
            Meaning = mean;
            Priority = priority;
            LowTrustProximity = lowTrustProximity;
            StopMeanings = stopMeanings;
        }
        public MeanFitRule() { }
        public int Priority { get; set; }
        public Meaning Meaning { get; set; }
        public double LowTrustProximity { get; set; }
        public string[] StopMeanings { get; set; }
    }
}
