﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mean
{
    public class ComplexSense: Sense
    {
        public ComplexSense(Sense[] orderedSenses, Meaning meaning, string origin, string normalized, double proximity)
            : base(meaning, normalized, origin, proximity)
        {
            this.OrderedChild = orderedSenses;
        }

        public Sense[] OrderedChild { get; protected set; }

    }
}
