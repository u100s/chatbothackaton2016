﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mean
{
    public class Sense
    {
        protected Sense() { }
        public Sense(Meaning meaning, string normalized, string origin, double proximity)
        {
            this.Meaning        = meaning;
            this.Normalized     = normalized;
            this.Origin         = origin;
            this.Proximity      = proximity;
        }
        public Meaning Meaning { get; protected set; }
        public string Normalized { get; protected set; }
        public string Origin { get; protected set; }
        public double Proximity { get;  set; }

        public bool HasMeaning()
        {
            if (Meaning == Meaning.Unknown)
                return false;
            if (Meaning == Meaning.Empty)
                return false;
            if (Meaning == Meaning.Abracadabra)
                return false;
            return true;
        }
    }
}
