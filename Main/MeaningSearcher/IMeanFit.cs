﻿using System;
namespace Mean
{
    public interface IMeanFit
    {
        MeanFitRule AddRuleIfItNotExistsOrNull(Meaning mean);
        MeanFitRule AddRuleIfItNotExistsOrNull(string id, params string[] keywords);
        MeanFitResult Fit(string origin, params MeanFitRule[] contextRules);
        MeanFitResult Fit(string origin, params string[] contextMeanings);
        IFitSearchBehaviour FitSearchBehaviour { get; set; }
        System.Collections.Generic.IEnumerable<Meaning> Meanings { get; }
        void SetFrom(System.Xml.Linq.XElement x);
        MeanFitRule this[string key] { get; set; }
    }
}
