﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mean
{
    public class MeanFitResult
    {
        protected MeanFitResult(double proximity, Meaning meaning)
        {
            Rule = null;
            IsUnknown = Rule == null;
            this.Meaning = meaning;
            IsProximityEnought = true;
            IsMeanfull = IsProximityEnought && !IsUnknown;
            Proximity = proximity;
            _tags = new MeanFitResult[0];

        }

        protected MeanFitResult(MeanFitRule rule, double proximity, MeanFitResult[] tags = null)
        {
            Rule = rule;
            IsUnknown = Rule == null;

            if(rule==null)
            {
                Meaning = Meaning.Unknown;
                IsProximityEnought = false;                
            }
            else
            {
                Meaning = rule.Meaning;
                IsProximityEnought = rule.LowTrustProximity <= proximity;
            }
            IsMeanfull = IsProximityEnought && !IsUnknown;
            Proximity = proximity;
            _tags = tags ?? new MeanFitResult[0];
            
        }
        public static MeanFitResult CreateTag(Meaning meaning){
            return new MeanFitResult(1, meaning);
        }
        public static MeanFitResult CreateUnknownResult(MeanFitResult[] tags = null)
        {
            return new MeanFitResult(null, 1, tags);
        }
        public static MeanFitResult Create(MeanFitRule rule, double proximity, MeanFitResult[] tags = null)
        {
            if (rule == null)
                throw new ArgumentNullException("Rule cannot be null. Use CreateUnknownResult instead");
            return new MeanFitResult(rule, proximity, tags);
        }
        public MeanFitRule Rule { get; protected set; }
        public Meaning Meaning { get; protected set; }
        public bool IsUnknown { get; protected set; }
        public bool IsProximityEnought { get; protected set; }
        public bool IsMeanfull { get; protected set; }
        public double Proximity { get; protected set; }
        MeanFitResult[] _tags = null;
        public MeanFitResult[] Tags { get { return _tags; } 
            set {
                if (value == null)
                    throw new ArgumentNullException("Tags array cannot be null");
                _tags = value;
            }
        }

        public bool Is(string meaningKey, bool proximityCheck = true)
        {

            if (IsUnknown)
                return false;
            if (proximityCheck && !IsProximityEnought)
                return false;
            
            var normalizedKey = Tools.PrepareForParsing(meaningKey);
            return Meaning.Id == normalizedKey;
        }
        public bool Is(MeanFitRule rule, bool proximityCheck = true)
        {
            if (IsUnknown)
                return false;
            if (proximityCheck && !IsProximityEnought)
                return false;
            return Rule == rule;
        }
        public bool Is(Meaning mean, bool proximityCheck = true)
        {
            if (IsUnknown)
                return false;
            if (proximityCheck && !IsProximityEnought)
                return false;
            return Rule.Meaning == mean;
         
        }
        public bool HasTagOrMeaning(string meaningKey)
        {
            return HasTag(meaningKey) || Is(meaningKey);
        }
        public bool HasTag(string meaningKey) {
            var normalizedTag = Tools.PrepareForParsing(meaningKey);
            return Tags.Any(t => t.Meaning.Id == normalizedTag);
        }
    }
}
