using System;

namespace Mean.Levenshtein
{
	/// <summary>
	/// Automaton state.
	/// </summary>
	public class AutomatonState {
		/// <summary>
		/// State number
		/// </summary>
		public int State;
		/// <summary>
		/// The offset.
		/// </summary>
		public int Offset;
	}
}

