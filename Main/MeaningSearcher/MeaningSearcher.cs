﻿using Mean.Levenshtein;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mean
{
    public class MeaningSearcher
    {
        readonly string[] separators = new[] { " ", ",", ".","-", "\t", "\r\n", "\n" };
        public MeaningSearcher(IEnumerable<Meaning> meanings)
        {
            Meanings = new List<Meaning>(meanings);
            SmallestTrustProximity = 0.5;
        }
        public MeaningSearcher()
        {
            Meanings = new List<Meaning>();
            SmallestTrustProximity = 0.5;
        }
        public double SmallestTrustProximity { get; set; }
        public IList<Meaning> Meanings { get; protected set; }

        double lowRangeCompetitionProximity = 0.6;
        double hiRangeCompetitionProximity  = 0.9;
        double abracadabraDecreaseEffect    = 0.9;
        double unknownDecreaseEffect        = 0.7;
        public Sense[] Search(string origin, bool summUnknown = true)
        {
            var layerResult = FirstLevelSearch(origin);
            if (layerResult.Length == 1)
                return layerResult;
            if (layerResult.Length == 0)
                throw new InvalidOperationException();

            while (true)
            {
                var newLayerResult = LevelUp(layerResult, origin, !summUnknown);
                if (newLayerResult.Length == 1)
                    return newLayerResult;
                if (newLayerResult.Length == layerResult.Length)
                    return newLayerResult;
                layerResult = newLayerResult;
            }
        }
        public Sense SearchOrNull(string origin, double trustProximity = 0.7)
        {
            var senses = Search(origin);
            if(senses.Length==0)
                return null;
            if(senses.Length>1)
                return null;
            if (senses.First().Meaning == Meaning.Unknown)
                return null;
            if (senses.First().Proximity < trustProximity)
                return null;
            return senses.First();
        }

        Sense[] FirstLevelSearch(string origin)
        {
            var senses = new List<Sense>();

            if (string.IsNullOrWhiteSpace(origin))
                return new[] { Meaning.Empty.MakeSense(origin, string.Empty) };

            var splitted = origin.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            var groupedByWordsCount =  splitOrder(Meanings.SelectMany(m => m.Keywords).ToArray());
            
            for(int i = 0; i< splitted.Length;){
                bool searched = false;
                foreach(var group in groupedByWordsCount){
                    if(i+ group.WordsCount>splitted.Length)
                        continue;
                    var substr = string.Concat(splitted.Skip(i).Take(group.WordsCount).Select(s=>s+" ")).Trim();
                    var sense = MakeSense(group.Corrector, substr);
                    if(group.WordsCount==1 || sense.Meaning!= Meaning.Unknown){
                        senses.Add(sense);
                        i+=group.WordsCount;
                        searched = true;
                        break;
                    }
                }
                if(!searched){
                    senses.Add(Meaning.Unknown.MakeSense(splitted[i], splitted[i], 1));
                    i++;
                }
            }

            return senses.ToArray();
        }

        Meaning getMeaningOf(string word)
        {
            return Meanings
                .FirstOrDefault(m => m.Keywords.Select(Tools.PrepareForParsing).Contains(word))
                ?? Meaning.Unknown;
        }
        Sense[] LevelUp(Sense[] senses, string origin, bool summUnknown )
        {
            //идём по цепи. Если что то неизвестно, то понижаем вероятностью. Игнорируем пустоту и абракадабру. стараемся объединить соседей
            //если что то неизвестно в цепи - то понижаем вероятность цепи
            var answer = new List<Sense>();

            Sense lastSense = null;
            foreach (var sense in senses)
            {
                if (lastSense == null)
                    lastSense = sense;
                else {
                    var summ = SummOrNull(lastSense, sense, summUnknown);
                    if(summ!=null) {
                        answer.Add(summ);
                    }
                    else {
                        answer.Add(lastSense);
                        answer.Add(sense);
                    }
                    lastSense = null;

                }
            }
            
            if (lastSense != null)
                answer.Add(lastSense);

            return answer.ToArray();
        }
        Sense SummOrNull(Sense a, Sense b, bool softSumm)
        {
            if (a.Meaning == b.Meaning)
            {
                var maxP = Math.Min(a.Proximity, b.Proximity);
                maxP += (1 - maxP) / 2;
                return MakeSense(a, b, a.Meaning, maxP);
            }
            Sense meanFull = null;
            Sense second = null;
            
            if (a.HasMeaning()) {
                if (b.HasMeaning())
                    return null;
                meanFull = a;
                second = b;
            }
            else if(b.HasMeaning()){
                meanFull = b;
                second = a;
            }
            else{
                if (a.Meaning == Meaning.Empty) {
                    second = a;
                    meanFull = b;
                }
                else if (b.Meaning == Meaning.Empty) {
                    second = b;
                    meanFull = a;
                } 
                else if (a.Meaning == Meaning.Abracadabra) {
                    second = a;
                    meanFull = b;
                }
                else if (b.Meaning == Meaning.Abracadabra)
                {
                    second = b;
                    meanFull = a;
                }
                else
                {
                    second = a;
                    meanFull = b;
                }

            }
            if(second.Meaning== Meaning.Empty)
                return MakeSense(a, b, meanFull.Meaning, meanFull.Proximity);
            if (softSumm)
                return null;
            if(second.Meaning== Meaning.Abracadabra)
                return MakeSense(a,b, meanFull.Meaning, meanFull.Proximity * this.abracadabraDecreaseEffect);
            if(second.Meaning == Meaning.Unknown )
                return  MakeSense(a, b, meanFull.Meaning, meanFull.Proximity * this.unknownDecreaseEffect);
            return null;
        }
        Sense MakeSense(SpellChecker corrector, string word)
        {
            double efficient = 1;
            var normalized = Tools.PrepareForParsing(word);

            IList<string> results = new List<string>();

            if (corrector.HasWord(normalized))
            {
                results.Add(normalized);
            }

            if (!results.Any())
            {
                if (normalized.Length > 2)
                {
                    results = corrector.GetCorrections1T(normalized);
                    efficient = 0.9;
                }
                if (!results.Any())
                {
                    if (normalized.Length > 3)
                    {
                        results = corrector.GetCorrections2T(normalized);
                        efficient = 0.8;
                    }
                }
            }
            

            if (results.Any())
            {
                var internalSenses = new List<Sense>();
                var proximityStep = (hiRangeCompetitionProximity - lowRangeCompetitionProximity) / (double)results.Count;
                int num = 0;

                foreach (var result in results)
                {
                    var mean = getMeaningOf(result);
                    if (!internalSenses.Any(s => s.Meaning == mean))
                    {
                        double proximity = 0;
                        if (normalized == result)
                            proximity = 1;
                        else
                        {
                            proximity = hiRangeCompetitionProximity - proximityStep * num;
                        }
                        internalSenses.Add(
                            mean.MakeSense(word, result, proximity * efficient));
                    }

                    num++;
                }
                if (internalSenses.Count > 1)
                    return new SenseCandidate(
                        internalSenses.ToArray(),
                        internalSenses.First().Meaning,
                        word,
                        normalized,
                        internalSenses.First().Proximity);
                else
                    return internalSenses.First();
            }
            else if (results.Count == 1) {
                return getMeaningOf(results.First()).MakeSense(word, normalized, 1);
            }
            else {
                return Meaning.Unknown.MakeSense(word, normalized);
            }
            
        }
        ComplexSense MakeSense(Sense a, Sense b, Meaning meaning, double proximity)
        {
            return new ComplexSense(new[] { a, b }, 
                meaning,
                a.Origin+ " "+ b.Origin,
                a.Normalized + " " + b.Normalized, proximity);
        }
        

        SplitOrderResults[] splitOrder(string[] strings)
        {
            strings = strings.Select(s => s.Trim()).ToArray();

            var grouped = strings.GroupBy(s => s.Split(separators, StringSplitOptions.RemoveEmptyEntries).Length);
            return grouped
                .OrderByDescending(g => g.Key)
                .Select(g => 
                    new SplitOrderResults { 
                        WordsCount = g.Key, 
                        Results = g.ToArray(), 
                        Corrector = new SpellChecker(g.ToArray())
                    })
                    .ToArray();
                    
        }
    }

    class SplitOrderResults {

        public SplitOrderResults() { }
        
        public string[] Results;
        public int WordsCount;
        public SpellChecker Corrector;
    }
}
