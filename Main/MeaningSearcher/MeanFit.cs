﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Mean
{
    public class MeanFit : Mean.IMeanFit
    {
        public MeanFit() {
            _meanings = new List<Meaning>();
            _rules = new Dictionary<string, MeanFitRule>();
            FitSearchBehaviour = new AlgorythmicFitSearch();
        }
        public IFitSearchBehaviour FitSearchBehaviour { get; set; }
        List<Meaning> _meanings;
        Dictionary<string, MeanFitRule> _rules;
        public void SetFrom(XElement x)
        {
            foreach (var xMean in x.Elements("mean"))
            {
                var mean = new Meaning(xMean);
                var rule = AddRuleIfItNotExistsOrNull(mean);
                if(rule==null)
                    throw new FormatException("Mean \"" + mean.Id + "\" is duplicated in a XElement");
                var xSpecialRuleSettings = xMean.Element("rule");
                if (xSpecialRuleSettings != null)
                {
                    var xPriority = xSpecialRuleSettings.Attribute("priority");
                    if (!string.IsNullOrWhiteSpace(xPriority.Value))
                        rule.Priority = int.Parse(xPriority.Value);
                    var xProximity = xSpecialRuleSettings.Attribute("proximity");
                    if (!string.IsNullOrWhiteSpace(xProximity.Value))
                        rule.LowTrustProximity = int.Parse(xProximity.Value) / (double)100;
                    var stopMeanings = new List<string>();
                    foreach (var stop in xSpecialRuleSettings.Elements("not"))
                        stopMeanings.Add(Tools.PrepareForParsing(stop.Value));
                    rule.StopMeanings = stopMeanings.ToArray();
                }
            }
        }
        public IEnumerable<Meaning> Meanings { get { return _meanings; } }
        public MeanFitRule this[string key]
        {
            get {
                var normalized = Mean.Tools.PrepareForParsing(key);
                if (!_rules.ContainsKey(normalized))
                    return null;
                else
                    return _rules[normalized];
            }
            set {
                var normalized = Mean.Tools.PrepareForParsing(key);
                if (_rules.ContainsKey(normalized))
                {
                    if (value == null)
                        _rules.Remove(normalized);
                    else
                        _rules[normalized] = value;
                }
                else {
                    if (value == null)
                        throw new ArgumentNullException();
                    _rules.Add(normalized, value);
                }
            }
        }
        public MeanFitRule AddRuleIfItNotExistsOrNull(Meaning mean)
        {
            if (_rules.ContainsKey(mean.Id))
                return null;
            _meanings.Add(mean);
            var ans = new MeanFitRule(mean);
            _rules.Add(mean.Id, ans);
            return ans;
        }
        public MeanFitRule AddRuleIfItNotExistsOrNull(string id, params string[] keywords)
        {
            var normalized = Tools.PrepareForParsing(id);
            var mean = new Meaning(normalized, keywords.Select(k=>Tools.PrepareForParsing(k)).ToArray());
            return AddRuleIfItNotExistsOrNull(mean);
        }
        public MeanFitResult Fit(string origin, params MeanFitRule[] contextRules)
        {
            origin = Tools.PrepareForParsing(origin);
            var searcher = new MeaningSearcher(_meanings);
            
            foreach (var rule in contextRules)
            {
                if (!_meanings.Contains(rule.Meaning))
                    throw new InvalidOperationException("Rule.Mening has to exits in Meanings collection");
            }
            var senses = searcher.Search(origin);
            return FitSearchBehaviour.Fit(senses, contextRules);
        }
        public MeanFitResult Fit(string origin, params string[] contextMeanings)
        {
            var rules = contextMeanings
                .Select(m => _rules[Tools.PrepareForParsing(m)])
                .ToArray();
            return Fit(origin, rules);
        }
    }
}
