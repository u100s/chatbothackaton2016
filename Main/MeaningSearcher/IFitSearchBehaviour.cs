﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mean
{
    public interface IFitSearchBehaviour
    {
        MeanFitResult Fit(Sense[] orderedSenses, MeanFitRule[] contextRules);
    }
}
