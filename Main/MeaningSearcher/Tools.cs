﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Mean
{
    public static class Tools
    {
        public static string PrepareForParsing(string str) {
            str = str.Trim().ToLower().Replace('c','с').Replace('a','а');
            var r = new Regex("(.)(?<=\\1\\1)", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);
            return r.Replace(str, String.Empty);
        }
    }
}
