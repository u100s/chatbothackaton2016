﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mean
{
    public class AlgorythmicFitSearch: IFitSearchBehaviour
    {
        public AlgorythmicFitSearch()
        {
            AbracadabraEffect = 0.8;
            UnknownEffect = 0.7;
        }
        public double AbracadabraEffect { get; set; }
        public double UnknownEffect { get; set; }

        public MeanFitResult Fit(Sense[] orderedSenses, MeanFitRule[] contextRules)
        {
            double efficiency = 1;
            int countOfAbracadabra = orderedSenses.Count(s => s.Meaning == Meaning.Abracadabra);
            int countOfUnknown = orderedSenses.Count(s => s.Meaning == Meaning.Unknown);
            var meanfull = orderedSenses.Where(s => s.HasMeaning()).ToArray();
            if (meanfull.Length == 0)
            {
                return MeanFitResult.CreateUnknownResult();
            }
            double ap = 1 - countOfAbracadabra / (double)meanfull.Length;
            efficiency = efficiency * (ap * (1-AbracadabraEffect) + AbracadabraEffect);

            double up = 1 - countOfUnknown / (double)meanfull.Length;
            efficiency = efficiency * (up *(1- UnknownEffect)+ UnknownEffect);

            var candidates = new List<MeanFitResult>();
            foreach (var rule in contextRules)
            {
                var fited = orderedSenses.Where(s => s.Meaning == rule.Meaning);
                if (!fited.Any())
                    continue;
                var maxProx = fited.Max(f => f.Proximity) * efficiency;
                if (rule.LowTrustProximity > maxProx)
                    continue;
                if (rule.StopMeanings != null && rule.StopMeanings.Length > 0)
                {
                    if (orderedSenses.Any(s => rule.StopMeanings.Contains(s.Meaning.Id)))
                        continue;
                }
                candidates.Add(MeanFitResult.Create(rule, maxProx));
            }
            MeanFitResult answer = null;
            if (candidates.Count() == 0)
            {
                answer = MeanFitResult.CreateUnknownResult();
            }
            else if (candidates.Count() == 1)
            {
                answer = candidates.First();
            }
            else
            {
                var maxPriority = candidates.Max(c => c.Rule.Priority);
                var hiPriorityCandidate = candidates.Where(c => c.Rule.Priority == maxPriority);
                if (hiPriorityCandidate.Count() > 1)
                    answer = MeanFitResult.CreateUnknownResult();
                else
                    answer = hiPriorityCandidate.First();
            }
            answer.Tags = meanfull.Select(m => MeanFitResult.CreateTag(m.Meaning)).ToArray();
            return answer;
        }
    }
}
