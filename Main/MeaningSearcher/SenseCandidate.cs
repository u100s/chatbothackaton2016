﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mean
{
    public class SenseCandidate: Sense
    {
        public SenseCandidate(Sense[] senses, Meaning meaning, string origin, string normalized, double proximity)
            : base(meaning, normalized, origin, proximity) {
            this.Candidates = senses;
        }
        public Sense[] Candidates { get; protected set; } 
    }
}
