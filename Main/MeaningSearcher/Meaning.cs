﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Mean
{
    public class Meaning
    {
        static Meaning _abracadabra = new Meaning("<abracadabra>", new string[0]);
        public static Meaning Abracadabra {
            get { return _abracadabra; }
        }
        static Meaning _unknown = new Meaning("<unknown>", new string[0]);
        
        public static Meaning Unknown {
            get { return _unknown; }
        }
        
        static Meaning _empty = new Meaning("<empty>", new string[0]);
        public static Meaning Empty
        {
            get { return _empty; }
        }
        public Meaning(string id, string[] keywords)
        {
            Id = id;
            this.Keywords = keywords;
        }
        public Meaning(XElement x)
        {
            var xId = x.Attribute("id");
            if (!string.IsNullOrWhiteSpace(xId.Value))
                Id = xId.Value;
            else
                throw new FormatException("Fild \"id\" is neccessary");
            var keywords = new List<string>();
            foreach (var xKey in x.Elements("key")) {
                keywords.Add(Tools.PrepareForParsing(xKey.Value));
            }
            Keywords = keywords.ToArray();
        }
        public string Id { get; set; }
        public string[] Keywords { get; protected set; }

        public Sense MakeSense(string origin, string normalized, double proximity = 1)
        {
            return new Sense(this, normalized, origin, proximity);
        }

        public override string ToString() {
            return "Meaning \"" + Id + "\"";
        }
    }
}
