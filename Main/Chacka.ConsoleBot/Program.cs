﻿using Chacka.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chacka.ConsoleBot
{
    class Program
    {
        static void Main(string[] args)
        {

            var fitter = new Mean.MeanFit();
            fitter.AddRuleIfItNotExistsOrNull(new Mean.Meaning("привет", new[] { "привет", "хай", "здарова", "хай", "хей", "хой" }));
            fitter.AddRuleIfItNotExistsOrNull(new Mean.Meaning("как дела", new[] { "как дела", "чё как", "чё там"}));

            var Router = new UserRouter(() => new ConsoleInteractor())
            {
                 Fitter = fitter,
                 DefaultUnknownReaction = new DefaultUnknownReaction(),
                 StartupAnswers = new PossibleAnswer [] { 
                     new PossibleAnswer<HelloReaction> { Rule =  fitter["привет"]}, 
                     new PossibleAnswer<FuckReaction>  { Rule =  fitter["привет"]}, 
                     new PossibleAnswer<CancelReaction>{ Rule =  fitter["привет"]}, 
                 },
            };
            while (true)
            {
                Console.WriteLine();
                Console.Write("[User]:");
                var msg = Console.ReadLine();
                Console.WriteLine();                
                Router.Handle(msg, 42);


            }
        }
    }
}
