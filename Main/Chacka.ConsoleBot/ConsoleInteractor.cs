﻿using Chacka.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chacka.ConsoleBot
{
    public class ConsoleInteractor: IInteractor
    {
        public void SendMessage(string message)
        {
            Console.WriteLine("[bot] " + message);
        }

        public void SendTyping(int msec)
        {
            Console.WriteLine("[bot typing for "+ msec+ "]");
        }

        public long chatId {
            get;
            set;
        }
    }
}
