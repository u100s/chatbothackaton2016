﻿using Chacka.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;

namespace TeleBot
{
    class TeleInteractor:  IInteractor
    {
        Api api;
        public long chatId { get; set; }
        public TeleInteractor(Api api)
        {
            this.api = api;
        }
        public void SendMessage(string message)
        {
            api.SendTextMessage(chatId, message);
        }

        public void SendTyping(int msec)
        {
            api.SendChatAction(chatId, Telegram.Bot.Types.ChatAction.Typing);
        }
    }
}
