﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mean;

namespace Chacka.Core
{

    public class FaqReaction : BaseReaction
    {
        public override PossibleAnswer[] React(MeanFitResult result, ContextModel model)
        {
            Output.SendTyping(200);
            Output.SendMessage("Я бы очень хотел помочь тебе с этой проблемой, но пока только учусь. Позвони по телефону +7(965)000-00-00 и тебе ответит квалифицированный специалист.");
            return ConcatWithEverything(new PossibleAnswer[]
            {
                new PossibleAnswer<ByeReaction> {Rule = Fitter["bye"]}
            });
        }
    }
}
