﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chacka.Core
{
    public class ContextModel
    {
        public ContextModel(){
            LastReactions = new List<IReaction>();
    }
        public long Id { get; set; }
        public List<IReaction> LastReactions { get; set; }
    }
}
