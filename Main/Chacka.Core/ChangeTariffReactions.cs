﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mean;

namespace Chacka.Core
{

    public class ChangeTariffReaction : BaseReaction
    {
        public override PossibleAnswer[] React(MeanFitResult result, ContextModel model)
        {
            Output.SendTyping(200);
            Output.SendMessage("Ты хочешь подобрать более оптимальный тариф?");
            return ConcatWithEverything(new PossibleAnswer[]
            {
                new PossibleAnswer<OkChangeTariffReaction> {Rule = Fitter["callscount"]}
            });
        }
    }

    public class OkChangeTariffReaction : BaseReaction
    {
        public override PossibleAnswer[] React(MeanFitResult result, ContextModel model)
        {
            Output.SendTyping(200);
            Output.SendMessage("Хорошо, сначала задам несколько вопросов:");
            Output.SendTyping(200);
            Output.SendMessage("Сколько звонков в день ты в среднем совершаешь?");
            return ConcatWithEverything(new PossibleAnswer[]
            {
                new PossibleAnswer<CallsCountReaction> {Rule = Fitter["callscount"]}
            });
        }
    }

    public class CallsCountReaction : BaseReaction
    {
        public override PossibleAnswer[] React(MeanFitResult result, ContextModel model)
        {
            Output.SendTyping(200);
            Output.SendMessage("Понял. А интернетом пользуешься?");
            return ConcatWithEverything(new PossibleAnswer[]
            {
                new PossibleAnswer<InternetYesReaction> {Rule = Fitter["internetyes"]},
                new PossibleAnswer<InternetNoReaction> {Rule = Fitter["internetno"]}
            });
        }
    }

    public class InternetYesReaction : BaseReaction
    {
        public override PossibleAnswer[] React(MeanFitResult result, ContextModel model)
        {
            Output.SendTyping(200);
            Output.SendMessage("Сколько гигов качаешь в месяц?");
            return ConcatWithEverything(new PossibleAnswer[]
            {
                new PossibleAnswer<InternetGigoffReaction> {Rule = Fitter["internetgigoff"]}
            });
        }
    }

    public class InternetGigoffReaction : BaseReaction
    {
        public override PossibleAnswer[] React(MeanFitResult result, ContextModel model)
        {
            Output.SendTyping(200);
            Output.SendMessage("Рекомендую тариф \"Всё за 600\" постоплатный. В нём включено 10Гб интернета, 600 минут разговора по Москве, много смсок, которыми никто не пользуется, и до 5-ти симок на оодин номер");
            return ConcatWithEverything(new PossibleAnswer[]
            {
                new PossibleAnswer<TariffYesReaction> {Rule = Fitter["tariffyes"]},
                new PossibleAnswer<TariffNoReaction> {Rule = Fitter["tariffno"]}
            });
        }
    }

    public class InternetNoReaction : BaseReaction
    {
        public override PossibleAnswer[] React(MeanFitResult result, ContextModel model)
        {
            Output.SendTyping(200);
            Output.SendMessage("Тогда рекомендую тариф «ВСЁ в одном»");
            Output.SendTyping(300);
            Output.SendMessage("Это безлимитная мобильная связь, Домашний интернет и ТВ в одном предложении. Подключай мобильный тариф «ВСЁ!» и плати за Домашний интернет и ТВ всего 1 рубль в месяц. Интернет и ТВ за 1 рубль — это не акция!Это навсегда! При переходе от другого оператора номер сохраняется.");
            return ConcatWithEverything(new PossibleAnswer[]
            {
                new PossibleAnswer<TariffYesReaction> {Rule = Fitter["tariffyes"]},
                new PossibleAnswer<TariffNoReaction> {Rule = Fitter["tariffno"]}
            });
        }
    }

    public class TariffYesReaction : BaseReaction
    {
        public override PossibleAnswer[] React(MeanFitResult result, ContextModel model)
        {
            Output.SendTyping(300);
            Output.SendMessage("Хорошо. Для подключения тарифа нужно прийти с паспортом к нам в офис. Ближайший к Вам офис находится по адресу Берсеневская набережная, дом 6 корпус 3. Четвёртый этаж, Digital October");
            return ConcatWithEverything(new PossibleAnswer[]
            {
                new PossibleAnswer<ByeReaction> {Rule = Fitter["bye"]}
            });
        }
    }

    public class TariffNoReaction : BaseReaction
    {
        public override PossibleAnswer[] React(MeanFitResult result, ContextModel model)
        {
            Output.SendTyping(200);
            Output.SendMessage("Хорошо. Тогда давай посмотрим другие варианты");
            Output.SendTyping(200);
            Output.SendMessage("Второй по популярности \"Добро пожаловать\". Стоимость минут 0 рублей, смс 0 рублей, абонентская плата 0 рублей");
            return ConcatWithEverything(new PossibleAnswer[]
            {
                new PossibleAnswer<TariffYesReaction> {Rule = Fitter["callscount"]}
            });
        }
    }

    public class ByeReaction : BaseReaction
    {
        public override PossibleAnswer[] React(MeanFitResult result, ContextModel model)
        {
            Output.SendTyping(200);
            Output.SendMessage("Рад был помочь! Обращайся, если ещё потребуюсь - я всегда тут.");
            return ConcatWithEverything(new PossibleAnswer[]
            {});
        }
    }

}
