﻿using Mean;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chacka.Core
{
    public class UserRouter
    {
        public UserRouter(Func<IInteractor> interactorLocator)
        {
            _interactorLocator = interactorLocator;
            _handlers = new Dictionary<long, UserHandler>();
        }
        public IReaction DefaultUnknownReaction { get; set; }
        public PossibleAnswer[] StartupAnswers { get; set; }
        Func<IInteractor> _interactorLocator;
        Dictionary<long, UserHandler> _handlers;
        public IMeanFit Fitter { get; set; }
        
        public void Handle(string msg, long id)
        {
            if (!_handlers.ContainsKey(id))
                _handlers.Add(id, CreateHandler(id));
            var hand = _handlers[id];
            hand.HandleMessage(msg);
        }
        UserHandler CreateHandler(long id)
        {
            var ans = new UserHandler
            {
                Interactor = _interactorLocator(),
                Context = new ContextModel { Id = id },
                Startup = StartupAnswers,
                DefaultUnknownReaction = DefaultUnknownReaction,
                Fitter = Fitter,
            };
            return ans;
        }
    }
}
