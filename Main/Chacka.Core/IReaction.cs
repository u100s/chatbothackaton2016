﻿using Mean;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chacka.Core
{

    public interface IReaction {
        IMeanFit    Fitter { get; set; }
        IInteractor Output {get;set;}
        MeanFitResult FitResult { get; set; }
        PossibleAnswer[] React(MeanFitResult result, ContextModel model);
    }


    public class PossibleAnswer<T> : PossibleAnswer where T : IReaction
    {

        public PossibleAnswer()
        {
            Callback = typeof(T);
        }
    }
    public class Jump : PossibleAnswer
    {
        public MeanFitResult PreviousResult { get; set; }
        public Jump(Type type, MeanFitResult PreviousResult )
        {
            this.Callback = type;
            this.PreviousResult = PreviousResult;
        }
    }
    public class PossibleAnswer
    {
        public Type Callback { get; set; }
        public MeanFitRule Rule { get; set; }
        public string KeyboardKeyOrNull { get; set; }
    }


}
