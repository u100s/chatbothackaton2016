﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chacka.Core
{
    public interface IInteractor
    {
        long chatId { get; set; }
        void SendMessage(string message);
        void SendTyping(int msec);
    }
}
