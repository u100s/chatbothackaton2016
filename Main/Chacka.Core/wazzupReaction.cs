﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chacka.Core
{
    public class wazzupReaction: IReaction
    {
        public Mean.IMeanFit Fitter
        {
            get;
            set;
        }

        public IInteractor Output
        {
            get;
            set;
        }

        public Mean.MeanFitResult FitResult
        {
            get;
            set;
        }

        public PossibleAnswer[] React(Mean.MeanFitResult result, ContextModel model)
        {
            Output.SendMessage("всё отлично");
            return null;
        }
    }
}
