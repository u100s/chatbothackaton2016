﻿using Mean;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chacka.Core
{
    public class HelloReaction : BaseReaction
    {

        public override PossibleAnswer[] React(MeanFitResult meanFitResult, ContextModel model)
        {
            Output.SendTyping(200);
            Output.SendMessage("Привет, дорогой клиент!");

            return ConcatWithEverything(new PossibleAnswer[]
            {
                new PossibleAnswer<ChangeTariffReaction> {Rule = Fitter["changetariff"]},
                new PossibleAnswer<FaqReaction>          {Rule = Fitter["faq"]}
            });
        }
    }
}
