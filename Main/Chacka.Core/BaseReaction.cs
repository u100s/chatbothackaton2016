﻿using Mean;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chacka.Core
{
    public class BaseReaction : IReaction
    {
        public IInteractor Output { get; set; }
        public IMeanFit Fitter { get; set; }
        public MeanFitResult FitResult { get; set; }

        public virtual PossibleAnswer[] React(MeanFitResult result, ContextModel model)
        {
            Output.SendMessage("Привет");
            Output.SendTyping(123);
            return new PossibleAnswer[] { new PossibleAnswer<wazzupReaction>() { Rule = Fitter["как дела"] } };
        }

        protected PossibleAnswer[] EverythingAnswers;

        public BaseReaction()
        {
            EverythingAnswers = new PossibleAnswer[]
            {
                new PossibleAnswer<FuckReaction> { Rule = Fitter["fuck"]},
                new PossibleAnswer<CancelReaction> { Rule = Fitter["cancel"]}
            };
        }

        protected PossibleAnswer[] ConcatWithEverything(PossibleAnswer[] reactions)
        {
            var result = new PossibleAnswer[EverythingAnswers.Length + reactions.Length];
            EverythingAnswers.CopyTo(result, 0);
            reactions.CopyTo(result, EverythingAnswers.Length);
            return result;
        }
    }
}
