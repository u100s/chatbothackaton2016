﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chacka.Core
{
    public class DefaultUnknownReaction: IReaction
    {
        public Mean.IMeanFit Fitter { get; set; }
        public IInteractor Output { get; set; }
        public PossibleAnswer[] React(Mean.MeanFitResult result, ContextModel model)
        {
            Output.SendTyping(1000);
            Output.SendMessage("Я вас не поняль...");
            var  lastModel = model.LastReactions.LastOrDefault();
            if(lastModel==null)
                return null;
            else
                return new PossibleAnswer[]{new Jump(lastModel.GetType(), lastModel.FitResult)};
        }

        public Mean.MeanFitResult FitResult{get;set;}
    }
}
