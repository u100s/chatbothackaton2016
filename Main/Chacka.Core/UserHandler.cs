﻿using Mean;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chacka.Core
{
    public class UserHandler
    {
        public IReaction DefaultUnknownReaction { get; set; }
        public IInteractor Interactor { get; set; }
        public IMeanFit Fitter { get; set; }
        public ContextModel Context { get; set; }
        public PossibleAnswer[] Startup { get; set; }
        PossibleAnswer[] _answers = null;
        public void HandleMessage(string message)
        {
            Interactor.chatId = Context.Id;
            PossibleAnswer[] ways = null;
            if (_answers == null || !_answers.Any())
            {
                ways = Startup;
            }
            else
            {
                ways = _answers;
            }

            var parsed = Fitter.Fit(message, ways.Select(w=>w.Rule).ToArray());
            
            var way = ways.FirstOrDefault(w=>w.Rule==parsed.Rule);
            
            HandleWay(way, parsed);
        }
        
        void HandleWay(PossibleAnswer way, MeanFitResult parsed)
        {
            IReaction currentReaction = DefaultUnknownReaction;
            
            if (way != null)
            {
                currentReaction = Activator.CreateInstance(way.Callback) as IReaction;
            }
            
            
            currentReaction.Fitter = Fitter;
            currentReaction.Output = Interactor;
            if (way is Jump) {
                currentReaction.FitResult = (way as Jump).PreviousResult;
            } 
            else {
                currentReaction.FitResult = parsed;
            }
            
            var result = currentReaction.React(parsed, Context);
            //выход
            if (result == null || !result.Any()) {
                _answers = new PossibleAnswer[0];
                Context.LastReactions.Clear();
            }
            else if (result[0] is Jump) {
                var jmp = result[0] as Jump;
                HandleWay(way, parsed);
            }
            else {
                _answers = result;
                Context.LastReactions.Add(currentReaction);
            }
        }
    }
}
