﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mean;

namespace Chacka.Core
{
    public class FuckReaction : IReaction
    {
        public IMeanFit Fitter { get; set; }
        public IInteractor Output { get; set; }
        public MeanFitResult FitResult { get; set; }

        public PossibleAnswer[] React(MeanFitResult result, ContextModel model)
        {
            Output.SendTyping(100);
            Output.SendMessage("И тебе желаю того же!");
            return new[]
            {
                new PossibleAnswer<ChangeTariffReaction> { Rule = Fitter["changetariff"] }
            };
        }
    }
    public class CancelReaction : IReaction
    {
        public IMeanFit Fitter { get; set; }
        public IInteractor Output { get; set; }

        public PossibleAnswer[] React(MeanFitResult result, ContextModel model)
        {
            Output.SendTyping(100);
            Output.SendMessage("Ок, если я понадоблюсь - только свистни!");
            return new[]
            {
                new PossibleAnswer<ChangeTariffReaction> { Rule = Fitter["changetariff"] }
            };
        }

        public MeanFitResult FitResult{get;set;}
    }
}
