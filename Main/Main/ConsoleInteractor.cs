﻿using Chacka.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main
{
    public class ConsoleInteractor: IInteractor
    {
        public void SendMessage(string message)
        {
            Console.WriteLine("Bot: " + message);
        }

        public void SendTyping(int msec)
        {

        }
    }
}
